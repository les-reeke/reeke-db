CREATE TABLE USERS (
    login varchar(255) CONSTRAINT PK_USERS PRIMARY KEY,
    password text NOT NULL,
    firstname varchar(255) NOT NULL,
    lastname varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    home_id integer,
    work_id integer
);