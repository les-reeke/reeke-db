CREATE TABLE LOCATIONS (
    id serial CONSTRAINT PK_LOCATIONS PRIMARY KEY,
    adress varchar(255) NOT NULL,
    postalcode integer,
    municipalityinseecode integer,
    municipality varchar(255) NOT NULL,
    department varchar(255),
    country varchar(255) NOT NULL,
    latitude decimal NOT NULL,
    longitude decimal NOT NULL
);

ALTER TABLE USERS ADD CONSTRAINT FK_HOME FOREIGN KEY(home_id) REFERENCES LOCATIONS(id);
ALTER TABLE USERS ADD CONSTRAINT FK_WORK FOREIGN KEY(work_id) REFERENCES LOCATIONS(id);