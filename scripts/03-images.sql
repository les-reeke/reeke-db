CREATE TABLE IMAGES (
	id serial constraint PK_IMAGES PRIMARY KEY, 
	url text
);