CREATE TABLE DISHES (
	id serial CONSTRAINT PK_DISHES PRIMARY KEY, 
	name varchar(255),
	description text,
	price decimal,
	note decimal,
	category_id varchar(255)
);

ALTER TABLE DISHES ADD CONSTRAINT FK_DISHES_CATEGORY FOREIGN KEY(category_id) REFERENCES CATEGORIES(name);