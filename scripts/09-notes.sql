CREATE TABLE NOTES (
  user_id varchar(255) NOT NULL,
  restaurant_id integer,
  dish_id integer,
  note decimal NOT NULL
);

ALTER TABLE NOTES ADD CONSTRAINT PK_NOTES PRIMARY KEY(user_id, restaurant_id, dish_id);

ALTER TABLE NOTES ADD CONSTRAINT FK_NOTES_USER FOREIGN KEY(user_id) REFERENCES USERS(login);
ALTER TABLE NOTES ADD CONSTRAINT FK_NOTES_RESTAURANT FOREIGN KEY(restaurant_id) REFERENCES RESTAURANTS(id);
ALTER TABLE NOTES ADD CONSTRAINT FK_NOTES_DISH FOREIGN KEY(dish_id) REFERENCES DISHES(id);

ALTER TABLE NOTES ADD CONSTRAINT CHECK_DISH_OR_RESTAURANT CHECK (restaurant_id IS NOT NULL OR dish_id IS NOT NULL);
