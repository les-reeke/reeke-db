FROM postgres:alpine
ENV POSTGRES_DB=reekedb

COPY scripts/* /docker-entrypoint-initdb.d/